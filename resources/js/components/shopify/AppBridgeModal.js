import React from "react";
import { Provider, Context, Modal } from '@shopify/app-bridge-react';

function AppBidgeModal()  {
    return (
        <Provider config={window.shopify_app_bridge}>
            <Modal title="My modal" message="Hello world!" open />
        </Provider>
    );
}
const root = document.createElement('div');
document.body.appendChild(root);
ReactDOM.render(<AppBidgeModal />, root);
