import React from "react";
import {
    Switch,
    Route, BrowserRouter
} from "react-router-dom";

import Menu from "../layouts/menu";
import TokenIndex from '../../components/pages/token/index';
import CreateToken from '../../components/pages/token/create';
import Dashboard from "../pages/Dashboard";
import HowIsWorks from "../pages/HowItWorks";
import Installation from "../pages/Installation";
import Layouts from '../layouts';

export default function RoutePath(){
    return(
        <BrowserRouter>
            <Menu />
            <Switch>
                <Route exact path='/' component={Dashboard}/>
                <Route path='/create-token/:id' component={CreateToken}/>
                <Route path='/token-index' component={TokenIndex}/>
                <Route path='/how-it-works' component={HowIsWorks}/>
                <Route path='/installation' component={Installation}/>
            </Switch>
        </BrowserRouter>
    )
}

