import {Page, Card, DataTable, Button} from '@shopify/polaris';
function HowIsWorks(){
    return (
        <div className="how-its-work-main">
            <Page>
                <Card sectioned>
                    <p>Woomio Plugin allows Shop Owners to add and track purchases made with token/coupon (campaign/affiliate) combinations which we supply to our affiliates.</p>
                    <p>This allows us to accurately pay affiliates for their marketing contributions and track the success of token/coupon (campaign/affiliate) combinations.</p>
                    <p>Each time a purchase is made with a token/coupon combination the data is sent to our API and stored in our database.</p>
                    <p><b>To setup the app:</b></p>
                    <ul>
                        <li>Please add a discount coupon (per affiliate) via the Shopify Discount section in your admin portal.</li>
                        <li>Add each coupon code to each one of your affiliates in your Woomio portal.</li>
                        <li>Then when setting up a campaign in Woomio you will recieve a token.</li>
                        <li>Navigate to the Woomio app in Shopify to add the token and set a ttl (time to live).</li>
                        <li>You can then generate a link in Woomio for each token/coupon combination to supply to the affiliates.</li>
                    </ul>
                </Card>
            </Page>
        </div>
    );
}
export default HowIsWorks;
