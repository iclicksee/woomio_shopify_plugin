import {Card, Page, Heading} from "@shopify/polaris";

function Installation(){
    return (
        <div className="how-its-work-main">
                <Page>
                    <Card sectioned>
                        <Heading>Steps</Heading>
                        <p>1. Checkout Javascript</p>
                        <p>2. Enable Javascript / App Embed</p>
                        <p>3. Dashboard</p>
                        <p>4. Create Token</p>
                        <p>5. Use Token</p>
                    </Card>
                    <Card sectioned>
                        <Heading>1. Checkout Javascript</Heading>
                        <p>1. In the Admin section of your store, click on settings bottom left</p>
                        <p>2. Then click on checkout and scroll down to Additional Scripts</p>
                        <p>3. Add &lt;script type='text/javascript' src='https://plugins.woomio.com/js/woomio.js'&gt;&lt;/script&gt; to Order status page input and save</p>
                        {/* <img src="/images/static/token/woomio_js_1.png" class="installation-img"/>
                        <img src="/images/static/token/woomio_js_2.png" class="installation-img"/>
                        <img src="/images/static/token/woomio_js_3.png" class="installation-img"/> */}
                    </Card>
                    <Card sectioned>
                        <Heading>2. Enable Javascript / App Embed</Heading>
                        <p>1. In the Admin section of your store, click on Online store</p>
                        <p>2. Then next to current theme click on customize</p>
                        <p>3. Then bottom left click Theme settings</p>
                        <p>3. Top Right you will see App embeds, click on this and enable Woomio Affiliates</p>
                    </Card>
                    <Card sectioned>
                        <Heading>3. Dashboard</Heading>
                        <img src="/images/static/token/dashboard.png" class="installation-img"/>
                    </Card>
                    <Card sectioned>   
                        <Heading>4. Create Token</Heading>
                        <img src="/images/static/token/token-index.png" className="installation-img"/>
                        <img src="/images/static/token/token-create.png" className="installation-img"/>
                    </Card>
                    <Card sectioned> 
                        <Heading>5. Use Token</Heading>
                        <img src="/images/static/token/token-ex.png" className="installation-img"/>
                    </Card>
                </Page>
        </div>
    );
}
export default Installation;
