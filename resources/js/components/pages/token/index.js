import React, { useCallback, useState, useEffect } from "react";
import { Page, Card, DataTable, Button } from "@shopify/polaris";
import RoutePath from "../../routes/index";
import { useHistory, useParams, Link } from "react-router-dom";
import { Provider, Modal } from "@shopify/app-bridge-react";

import { Table, Input } from "antd";
import "antd/dist/antd.css";

function TokenIndex() {
    const history = useHistory();

    const [rows, setRows] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [deleteToken, setDeleteToken] = useState({ id: "", name: "" });
    const [primaryContent, setPrimaryContent] = useState("");

    const [search, setSearch] = useState("");
    const [filteredRows, setFilteredRows] = useState([]);

    const columns = [
        {
            title: "ID",
            dataIndex: "id",
        },
        {
            title: "Name",
            dataIndex: "name"
        },
        {
            title: "Campaign Name",
            dataIndex: "campaign_name",
        },
        {
            title: "Campaign URL",
            dataIndex: "campaign_url",
            render: (campaign_url) => <a href={campaign_url}>{campaign_url}</a>,
        },
        {
            title: "Expire Time (days)",
            dataIndex: "expire_time",
            defaultSortOrder: "ascend",
            sorter: (a, b) => a.expire_time - b.expire_time,
        },
        {
            title: "Action",
            dataIndex: "id", 
            render: (id, name) => (
                <>
                    <Button
                        className="woomio_primary_btn"
                        onClick={() => editToken(id)}
                    >
                        <i
                            className="fa fa-pencil-square-o"
                            aria-hidden="true"
                        ></i>
                    </Button>
                    <Button
                        className="woomio_primary_btn"
                        onClick={() => confirmRemoveToken(id, name)}
                    >
                        <i className="fa fa-trash-o" aria-hidden="true"></i>
                    </Button>
                </>
            ),
        },
    ];

    useEffect(() => {
        getData();
    }, []);

    async function getData() {
        const response = fetch("/token", {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        })
            .then((response) => response.json())
            .then((res) => {
                if (res.isSuccess) {
                    setRows(res.data);
                    setFilteredRows(res.data)
                } else {
                    console.log("ERROR:: " + res.data);
                }
            });
    }

    function editToken(id) {
        history.push("/create-token/" + id);
    }

    async function confirmRemoveToken(id, name) {
        setDeleteToken({ id: id, name: name });
        setPrimaryContent("Are you sure to delete '" + name + "' token?");
        setShowModal(true);
    }

    async function removeToken() {
        let id = deleteToken.id;
        const response = await fetch("/token/" + id, {
            method: "DELETE",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        })
            .then((response) => response.json())
            .then((res) => {
                if (res.isSuccess) {
                    setShowModal(false);
                    getData();
                } else {
                    console.log("ERROR:: " + res.data);
                }
            });
    }

    function onActionCallback(value) {
        console.log(value);
    }

    function onCloseCallback(value) {
        setShowModal(false);
    }

    return (
        <div className="token-index-main">
            <Modal
                title="Remove Token!"
                message={primaryContent}
                open={showModal}
                primaryAction={{
                    content: "OK",
                    onAction: removeToken,
                }}
                secondaryActions={[
                    {
                        content: "Cancel",
                        onAction: onCloseCallback,
                    },
                ]}
                onClose={onCloseCallback}
                onAction={(value) => onActionCallback(value)}
            />

            <Page title="Tokens">

                <div className="my-4">

                  <div className="text-left" style={{float: "left", width:"300px"}}>
                    <Input
                      placeholder="Search Name"
                      value={search}
                      onChange={e => {
                        const currValue = e.target.value;
                        setSearch(currValue);
                        const filteredData = rows.filter(row =>
                          row.name.includes(currValue)
                        );
                        setFilteredRows(filteredData);
                      }}
                    />
                  </div>

                  <div className="text-right">
                    <Link to="/create-token/0">
                        <Button primary>Create Token</Button>
                    </Link>
                  </div>

                </div>

                <Table 
                  dataSource={filteredRows} 
                  rowKey="id" 
                  columns={columns} 
                />

            </Page>
        </div>
    );
}
export default TokenIndex;
