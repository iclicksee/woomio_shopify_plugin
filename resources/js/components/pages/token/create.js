import React, {useCallback, useEffect, useState} from 'react';
import {Page, Button} from '@shopify/polaris';
import {Toast} from '@shopify/app-bridge-react';

import { useHistory, useParams } from "react-router-dom";
import {Link} from "react-router-dom";

import { Form, Select, Input, InputNumber, Row, Col, Spin } from 'antd';
import 'antd/dist/antd.css';
const { Option } = Select;

function CreateToken()  {

    const history = useHistory();
    const { id } = useParams();
    const [form] = Form.useForm();

    const [tokens, setTokens] = useState([
      {id: '', name: '', expire_time: 1, campaign_name: '', campaign_url: '', discount_codes:[] }
    ]);
    
    const [loading, setLoading] = useState(false);
    const [coupons, setCoupons] = useState([]);

    const [showToast, setShowToast] = useState(false);
    const [errors, setErrors] = useState([]);

    const toggleActive = useCallback(() => setShowToast((showToast) => !showToast), []);
    
    const toastMarkup = showToast ? (
        <Toast content="Message sent" onDismiss={toggleActive} />
    ) : null;


    useEffect(() => {
        console.log('useEffect');
        // get data from serve of current plan to edit
        (async () => {

            console.log('id='+id);
            // Edit 
            if( id != 0 ){

                setLoading(true)
                console.log('Get Coupons');
                // Getting Coupons data to populate select
                const coupons = await fetch('/shopifycoupons/' + id)
                .then(response => response.json())
                .then(res => {
                    if (res.isSuccess) {
                      setCoupons(res.data);  

                      // Getting Token data to populate form
                      const response = fetch('/token/' + id + '/edit')
                      .then(response => response.json())
                          .then(res =>{
                              if (res.isSuccess) {

                                  // Manipulating data to get the needed results
                                  const result = res.data[0].discount_codes.map(({ shopify_id }) => (shopify_id) );
                                  res.data[0].discount_codes = result;

                                  // Populating Form
                                  form.setFieldsValue(res.data[0]);
                                  setLoading(false)
      
                              } else {
                                  console.log('ERROR:: ' + res.data);
                              }
                          })
                    } else {
                        console.log('ERROR:: ' + res.data);
                    }
                })    
            } else {
              
              // Get Available Coupons
              getCoupons();
            
            }

        })();
        
    }, []);

    // function addToken(){
    //     var array = [...tokens]; // make a separate copy of the array
    //     array.push({id: '', name: ''});
    //     setTokens(array);
    // }

    // function removeToken(index){
    //     var array = [...tokens]; // make a separate copy of the array
    //     array.splice(index, 1);
    //     setTokens(array);
    // }

    // function successToast(message){
    //     const toastNotice = Toast.create(window.shopify_app_bridge, {
    //         message: message,
    //         duration: 5000
    //     });
    //     toastNotice.dispatch(Toast.Action.SHOW);
    // }

    const getCoupons = useCallback(async ()  => {

        const response = await fetch('/coupon')
        .then( response => response.json() )
          .then( res => {
            if (res.isSuccess) {
                setCoupons(res.data)
            } else {
                console.log('ERROR:: ' + res.data);
                setErrors(res);
            }
        }).catch( err =>{
            setErrors(err);
            console.log(errors);
        });
    });


    const saveToken = useCallback(async (values)  => {

        let data = [values];

        const response = await fetch('/token', {
            method: 'POST', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            body: JSON.stringify(data)
        })
        .then( response => response.json()).then(res => {
            if (res.isSuccess) {
                history.push('/token-index')
            } else {
                console.log('ERROR:: ' + res.data);
                setErrors(res);
            }
        })
        .catch( err =>{
            setErrors(err);
            console.log(errors);
        });

    });

    const resetForm = () => {
      form.resetFields();
    };
  
    return (
        <Page>
          <Form
            form={form}
            layout='vertical'
            initialValues={tokens}
            onFinish={saveToken}
            autoComplete="off"
          >
            <div className="dashboard-main">
                
                <div className='text-left my-5'>
                  <Link className='primary_black_color' to='/token-index/'><i className="fa fa-arrow-left mr-3" aria-hidden="true"></i>Back</Link>
                </div>

                <div className="Polaris-Card">
                    <div className="Polaris-Card__Section pb-5">

                      { !loading
                      ?
                        <Row gutter={16}>

                          <Form.Item label="Id" name="id" hidden={true}>
                            <Input placeholder="Please insert Name" />
                          </Form.Item>
                          
                          <Col className="gutter-row" span={4}>
                            <Form.Item label="Name" name="name" rules={[{ required: true, message: 'Please insert Name' }]}>
                              <Input placeholder="Please insert Name" />
                            </Form.Item>
                          </Col>  

                          <Col className="gutter-row" span={4}>    
                            <Form.Item label="Expiry Time (days)" name="expire_time" rules={[{ required: true, message: 'Please insert expiry time' }]}>
                              <InputNumber placeholder="Please insert expiry time" min={1} style={{ width: '100%' }} />
                            </Form.Item>
                          </Col>  

                          <Col className="gutter-row" span={4}>
                            <Form.Item label="Campaign Name" name="campaign_name">
                              <Input placeholder="Please insert Campaign Name" />
                            </Form.Item>
                          </Col>  

                          <Col className="gutter-row" span={4}>
                            <Form.Item label="Campaign URL" name="campaign_url">
                              <Input placeholder="Please insert Campaign URL" />
                            </Form.Item>
                          </Col>  

                          <Col className="gutter-row" span={6}>  
                            <Form.Item label="Select Coupon(s)" name="discount_codes" rules={[{ required: true, message: 'Please select at least one coupon' }]} >
                              <Select placeholder="Please select at least one coupon"
                                mode="multiple"
                                allowClear
                              >
                              {
                                coupons.map(coupon => (
                                  <Option key={coupon.id} value={coupon.id}>{coupon.code}</Option>
                                ))
                              }
                              </Select>
                            </Form.Item>
                          </Col>

                          <Col className="gutter-row" span={2}>  
                            {/* <Form.Item>
                              <Button
                                type="dashed"
                                onClick={() => add()}
                                style={{ width: '60%' }}
                              >
                              <PlusOutlined />
                              </Button>
                            </Form.Item> */}
                          </Col>

                        </Row>

                      :
                        <Spin/>
                      }

                      
                    </div>
                </div>

                <div className='text-right mt-3 pt-4'>
                  <Form.Item>
                    <Button submit={true} primary>Save</Button>
                    <Button onClick={resetForm}>Reset</Button>
                  </Form.Item>  
                  {toastMarkup}
                </div> 

            </div>
          </Form>
        </Page>
    );
}
export default CreateToken;
