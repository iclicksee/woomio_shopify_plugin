import React, {useCallback, useEffect, useState} from 'react';
import {TextField, Button, InlineError, Page} from '@shopify/polaris';
import {Toast} from '@shopify/app-bridge-react';
import RoutePath from "../../routes";
import { useHistory, useParams } from "react-router-dom";
import {Link} from "react-router-dom";

import { Select } from 'antd';
import 'antd/dist/antd.css';
const { Option } = Select;

function CreateToken()  {
    const { id } = useParams();
    const [tokens, setTokens] = useState([{id: '', campaign_name: '', campaign_url: '',name: '', expire_time: 2, coupons:[] }]);
    const [coupons, setCoupons] = useState([]);
    const [showToast, setShowToast] = useState(false);
    const [errors, setErrors] = useState([]);


    const toggleActive = useCallback(() => setShowToast((showToast) => !showToast), []);
    const toastMarkup = showToast ? (
        <Toast content="Message sent" onDismiss={toggleActive} />
    ) : null;
    const history = useHistory();

    useEffect(() => {
        // get data from serve of current plan to edit
        (async () => {

            // Edit 
            if( id != 0 ){
                const response = await fetch('/token/' + id + '/edit').
                then(response => response.json())
                    .then(res =>{
                        if (res.isSuccess) {
                            setTokens(res.data);
                            console.log(coupons)
                        } else {
                            console.log('ERROR:: ' + res.data);
                        }
                    })

                const coupons = await fetch('/shopifycoupons/' + id)
                .then(response => response.json())
                .then(res => {
                    if (res.isSuccess) {
                      setCoupons(res.data);
                    } else {
                        console.log('ERROR:: ' + res.data);
                    }
                })    
            } else {
              
              // Get Available Coupons
              getCoupons();
            
            }

        })();
        
    }, []);

    function handleChange(index, value, action){
        var array = [...tokens];
        if( action == 'name' ){
            array[index].name = value;
        }else if( action == 'expire_time' ){
            array[index].expire_time = value;
        } else if( action == 'campaign_url' ){
            array[index].campaign_url = value;
        } else if( action == 'campaign_name' ){
            array[index].campaign_name = value;
        } else if( action == 'discount_codes' ){
            array[index].discount_codes = value;
        }  

        setTokens(array);
    }

    function addToken(){
        var array = [...tokens]; // make a separate copy of the array
        array.push({id: '', name: ''});
        setTokens(array);
    }

    function removeToken(index){
        var array = [...tokens]; // make a separate copy of the array
        array.splice(index, 1);
        setTokens(array);
    }

    function successToast(message){
        const toastNotice = Toast.create(window.shopify_app_bridge, {
            message: message,
            duration: 5000
        });
        toastNotice.dispatch(Toast.Action.SHOW);
    }

    const getCoupons = useCallback(async ()  => {

        const response = await fetch('/coupon')
        .then( response => response.json() )
          .then( res => {
            if (res.isSuccess) {
                setCoupons(res.data)
            } else {
                console.log('ERROR:: ' + res.data);
                setErrors(res);
            }
        }).catch( err =>{
            setErrors(err);
            console.log(errors);
        });
    });


    const saveToken = useCallback(async ()  => {

        const response = await fetch('/token', {
            method: 'POST', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            body: JSON.stringify(tokens)
        }).then( response => response.json()).then(res => {
            if (res.isSuccess) {
                history.push('/token-index')
            } else {
                console.log('ERROR:: ' + res.data);
                setErrors(res);
            }
        }).catch( err =>{
            setErrors(err);
            console.log(errors);
        });
    }, [tokens]);

    return (
        <Page>
            <div className="dashboard-main">
                <div className='text-left my-5'>
                    <Link className='primary_black_color' to='/token-index/'><i className="fa fa-arrow-left mr-3" aria-hidden="true"></i>Back</Link>
                </div>
                    <div className="Polaris-Card">
                        <div className="Polaris-Card__Section pb-5">
                            { tokens.map((token,index) => {

                                console.log(token)
                                console.log(token.coupons.map(coupon => coupon.shopify_id))

                                return (
                                    <div>
                                        <div className="token_main">

                                            <TextField key={`token`+index} label="Token" value={token.name} onChange={value => handleChange(index, value, 'name')} placeholder="Token123"/>
                                            <TextField key={`expire_time`+index} label="Expire Time (in days)" value={token.expire_time} type="number" onChange={value => handleChange(index, value, 'expire_time')} placeholder="5"/>
                                            <TextField key={`campaign_url`+index} label="Campaign Url" value={token.campaign_url} onChange={value => handleChange(index, value, 'campaign_url')} placeholder="Campaign URL"/>
                                            <TextField key={`campaign_name`+index} label="Campaign Name" value={token.campaign_name} onChange={value => handleChange(index, value, 'campaign_name')} placeholder="Campaign Name"/>

                                            <div>

                                              <label>Select Coupon(s)</label><br/>
                                              <Select
                                                mode="multiple"
                                                allowClear
                                                loading={!coupons.length}
                                                style={{ width: '100%' }}
                                                placeholder="Select Coupon"
                                                value={token.coupons.map(coupon => coupon.shopify_id)}
                                                onChange={value => handleChange(index, value, 'discount_codes')}
                                              >
                                                { coupons.map(coupon => (
                                                    <Option key={coupon.id} value={coupon.id}>
                                                      {coupon.code}
                                                    </Option>
                                                  ))
                                                }
                                              </Select>
                                            </div>

                                            {/* 
                                            
                                            {(id == 0) ?
                                                <span>
                                                {(index == 0 ) ?
                                                    <Button onClick={addToken}>
                                                        <span key={`addtoken` + index}
                                                              className="Polaris-Icon Polaris-Icon--hasBackdrop add-token-dropdown"
                                                              aria-label="">
                                                            <svg className="Polaris-Icon__Svg" viewBox="0 0 55 55">
                                                            <path
                                                                d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M38.5,28H28v11c0,1.104-0.896,2-2,2  s-2-0.896-2-2V28H13.5c-1.104,0-2-0.896-2-2s0.896-2,2-2H24V14c0-1.104,0.896-2,2-2s2,0.896,2,2v10h10.5c1.104,0,2,0.896,2,2  S39.604,28,38.5,28z"
                                                                fill="#6C7BCF">
                                                            </path>
                                                        </svg>
                                                        </span>
                                                    </Button>
                                                    :
                                                    <Button>
                                                    <span onClick={() => removeToken(index)} key={`removetoken`+index} className="Polaris-Icon Polaris-Icon--hasBackdrop">
                                                        <svg className="Polaris-Icon__Svg" viewBox="0 0 55 55">
                                                            <path
                                                                d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M38.5,28h-25c-1.104,0-2-0.896-2-2  s0.896-2,2-2h25c1.104,0,2,0.896,2,2S39.604,28,38.5,28z"
                                                                fill="#ff0000">
                                                            </path>
                                                        </svg>
                                                    </span>
                                                    </Button>
                                                }
                                            </span>
                                                : null
                                            } 
                                            
                                            */}

                                        </div>
                                        {(errors[index + '.name']) ?
                                            <InlineError message="required" fieldID="myFieldID" />
                                            : null
                                        }
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                <div className='text-right mt-3 pt-4'>
                    <Button onClick={saveToken} primary>Save</Button>
                    {toastMarkup}
                </div>
            </div>
        </Page>
    );
}
export default CreateToken;
