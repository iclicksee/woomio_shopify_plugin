import React, {useCallback, useState, useEffect} from "react";
import {Page, Card, Pagination} from '@shopify/polaris';

import { Table, Input } from "antd";
import "antd/dist/antd.css";

function Dashboard()  {
    const [rows, setRows] = useState([]);
    const [search, setSearch] = useState("");
    const [filteredRows, setFilteredRows] = useState([]);

    const columns = [
      {
          title: "Token",
          dataIndex: "token",
      },
      {
          title: "Coupon",
          dataIndex: "coupon"
      },
      {
          title: "# of Purchases",
          dataIndex: "no_of_purchase",
          defaultSortOrder: "descend",
          sorter: (a, b) => a.no_of_purchase - b.no_of_purchase,
      },
      {
          title: "Total Purchase Amount",
          dataIndex: "total_purchase_amount",
          defaultSortOrder: "descend",
          sorter: (a, b) => a.no_of_purchase - b.no_of_purchase,
          render: (purchase_currency, total_purchase_amount) => <span>{purchase_currency} {total_purchase_amount}</span> 
      }
  ];


    useEffect(() => {
        getData('/dashboard?page=1');
    }, []);

    async function getData(url){
        const response = fetch(url, {
            method: 'GET', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).then(response => response.json())
            .then(res =>{
                if (res.isSuccess) {
                    setRows(res.data.data);
                    setFilteredRows(res.data.data)
                } else {
                    console.log('ERROR:: ' + res.data);
                }
            })

    }

    return (
        <Page title="Dashboard">

            <div className="my-4">

                <div className="text-left" style={{width:"300px"}}>
                  <Input
                    placeholder="Search Token"
                    value={search}
                    onChange={e => {
                      const currValue = e.target.value;
                      setSearch(currValue);
                      const filteredData = rows.filter(row =>
                        row.token.includes(currValue)
                      );
                      setFilteredRows(filteredData);
                    }}
                  />
                </div>

            </div>

            <Table 
              dataSource={filteredRows} 
              rowKey="id" 
              columns={columns} 
            />
        </Page>
    );
}
export default Dashboard;
