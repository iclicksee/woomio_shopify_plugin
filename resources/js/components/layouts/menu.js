import RoutePath from "../routes/index";
import {Link} from "react-router-dom";

import {Card, Tabs} from '@shopify/polaris';
import Dashboard from "../pages/Dashboard";
import React, {useCallback, useState, useEffect} from "react";
import { useHistory, useParams } from "react-router-dom";

function Menu() {
    const history = useHistory();
    const [selected, setSelected] = useState(0);

    const tabs = [
        {
            id: 'dashboard',
            content: 'Dashboard',
            accessibilityLabel: 'All customers',
            panelID: 'dashboard',
            path: '/'
        },
        {
            id: 'tokens',
            content: 'Tokens',
            panelID: 'tokens',
            path: '/token-index/'
        },
        {
            id: 'how-it-works',
            content: 'How it works',
            panelID: 'how-it-works',
            path: '/how-it-works/'
        },
        {
            id: 'installation',
            content: 'Installation',
            panelID: 'installation',
            path: '/installation/'
        },
    ];
    const handleTabChange = useCallback(
        (selectedTabIndex) => setSelected(selectedTabIndex),
        [],

        history.push(tabs[selected].path)
    );
    return (
        <Card>
            <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange}>
            </Tabs>
        </Card>
    );
}

export default Menu;
