import {Toast} from '@shopify/app-bridge-react';

function Helper(){
    state = {
        showToast: true,
      };

    function initLoading(){
        loading = Loading.create(window.shopify_app_bridge);
    }
    function startLoading(){
        this.initLoading();
        loading.dispatch(Loading.Action.START);
    }
    function stopLoading(){
        this.initLoading();
        loading.dispatch(Loading.Action.STOP);
    }
    function successToast(message){
        const toastNotice = Toast.create(this.context.polaris.appBridge, {
              message: "Test Toast",
              duration: 5000
            });
            toastNotice.dispatch(Toast.Action.SHOW);
    }
    function errorToast(message){
        let toastNotice = Toast.create(window.shopify_app_bridge, {message:message,duration: 2000,isError: true});
        toastNotice.dispatch(Toast.Action.SHOW);
    }
    function confirmModel(){

        const okButton = Button.create(window.shopify_app_bridge, {label: 'Ok'});
        okButton.subscribe(Button.Action.CLICK, () => {
            console.log('Ok okButton');
        });
        const cancelButton = Button.create(window.shopify_app_bridge, {label: 'Cancel'});
        cancelButton.subscribe(Button.Action.CLICK, () => {
            console.log('Ok cancelButton');
        });
        const modalOptions = {
            title: 'My Modal',
            footer: {
                buttons: {
                    primary: okButton,
                    secondary: [cancelButton],
                },
            },
        };

        const myModal = Modal.create(window.shopify_app_bridge, modalOptions);

        myModal.subscribe(Modal.Action.OPEN, () => {
            console.log('Ok OPEN');
        });

        myModal.subscribe(Modal.Action.CLOSE, () => {
            console.log('Ok CLOSE');
        });
    }

     function contextualSaveBar(options = {}){
        if(!options){
            options = {
                saveAction: {
                    disabled: false,
                    loading: false,
                },
                discardAction: {
                    disabled: false,
                    loading: false,
                    discardConfirmationModal: true,
                },
            };
        }
         return ContextualSaveBar.create(window.shopify_app_bridge, options);
    }

    function bannerHide(){
        let elem = document.getElementById("component-Banner");
        if(elem)
        {
            elem.style.display = "none";
        }
    }
    function bannerShow(){
       let elem = document.getElementById("component-Banner");
            if(elem)
            {
                elem.style.display = "block";
            }
    }
}

export default Helper;
