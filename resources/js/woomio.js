const host = process.env.MIX_APP_URL;
console.log('here');
const apiEndPoint = host + '/api';

function init() {
    const urlParams = new URLSearchParams(window.location.search);
    const token = urlParams.get('token');
    const coupon = urlParams.get('coupon');
    const shop = window.location.hostname;
    console.log(shop);
    if (token && coupon) {
        const Http = new XMLHttpRequest();
        const url = apiEndPoint + '/get-token?token=' + token + '&shop=' + shop;
        Http.open("GET", url);
        Http.send();

        Http.onreadystatechange = (e) => {
            let res = JSON.parse(Http.responseText);
            if (res.isSuccess) {
                if (res.data.expire_time == null || res.data.expire_time == '') {
                    document.cookie = 'woomio_token' + "=" + token + "; path=/";
                } else {
                    console.log('123');
                    var date = new Date();
                    date.setTime(date.getTime() + (res.data.expire_time * 24 * 60 * 60 * 1000));
                    var expiry = '; expires=' + date.toUTCString();
                    document.cookie = 'woomio_token' + "=" + token + expiry + "; path=/";
                    document.cookie = 'woomio_coupon' + "=" + coupon + expiry + "; path=/";
                }
                localStorage.setItem("woomio_token", token);
                localStorage.setItem("woomio_coupon", coupon);
            } else {
                var date = new Date();
                date.setTime(date.getTime() + (1000));
                var expiry = '; expires=' + date.toUTCString();
                document.cookie = 'woomio_token' + "=" + token + expiry + "; path=/";
                localStorage.setItem("woomio_token", '');
                localStorage.setItem("woomio_coupon", '');
            }
        }
    }
    setTimeout(function() {
        if (document.cookie.indexOf('woomio_token') == -1) {
            localStorage.setItem("woomio_token", '');
            localStorage.setItem("woomio_coupon", '');
        }

        if (localStorage.getItem("woomio_token") != '' && localStorage.getItem("woomio_coupon") != '') {
            let href = window.location.href;
            if (href.indexOf('thank_you') !== -1) {
                var order_id = Shopify.checkout.order_id;
                const Http = new XMLHttpRequest();
                const url = apiEndPoint + '/set-token?token=' + localStorage.getItem("woomio_token") + '&coupon=' + localStorage.getItem("woomio_coupon") + '&shop=' + shop + '&order_id=' + order_id;
                Http.open("GET", url);
                Http.send();
                Http.onreadystatechange = (e) => {
                    let res = JSON.parse(Http.responseText);
                    localStorage.setItem("woomio_token", '');
                    localStorage.setItem("woomio_coupon", '');
                }
            }
        }
    }, 200);
}

function buyNowButtonClick() {
    var token = (localStorage.getItem("woomio_token") != '') ? localStorage.getItem("woomio_token") : '';
    var coupon = (localStorage.getItem("woomio_coupon") != '') ? localStorage.getItem("woomio_coupon") : '';
    var attrs = "attributes[woomio_token]=" + token + "&attributes[woomio_coupon]=" + coupon;
    var selectedID = $("select[name='id']").val();

    var className = document.getElementsByClassName('shopify-payment-button__button')[0].className;
    var buttonText = document.getElementsByClassName('shopify-payment-button__button')[0].innerHTML;
    console.log(buttonText);
    $(".shopify-payment-button").prepend('<a href="/cart/' + selectedID + ':1?' + attrs + '" class="' + className + '">' + buttonText + '</a>');
    var elem = document.getElementsByClassName('shopify-payment-button__button')[1];
    elem.parentNode.removeChild(elem);
}

window.onload = init();