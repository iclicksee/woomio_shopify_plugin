<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\Token\TokenController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/login', [AuthController::class,'index']
)->name('login');

Route::get('/', function () {
    return view('layouts.app');
})->middleware(['auth.shopify'])->name('home');

Route::group(['middleware' => ['auth.shopify']], function () {
    Route::resource('dashboard', DashboardController::class);
    Route::resource('token', TokenController::class);
    Route::get('test', [TokenController::class, 'test']);
    Route::get('coupon', [TokenController::class, 'coupons']);
    Route::get('shopifycoupons/{id}', [TokenController::class, 'shopifycoupons']);
});
//Auth::routes();

Route::get('flush', function (){
    request()->session()->flush();
});
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
