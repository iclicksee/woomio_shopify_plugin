<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Api\AppController;
use App\Http\Controllers\Api\TokenController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cors']], function () {
    Route::group(['namespace' => 'Api'], function () {
        Route::get('get-token', [TokenController::class, 'getToken']);
        Route::get('set-token', [TokenController::class, 'setToken']);
    });
    Route::group(['prefix' => 'webhook'], function () {
        Route::post('app-uninstalled',[AppController::class,'destroy']);
    });
});
