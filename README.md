# Shopify Woomio App

### Installation
Install the dependencies and devDependencies.

For both environment
```sh
$ composer install
$ cp .env.example .env 
$ nano .env // set all credentials(ex: database, shopify api key and secret, mail credentials)
$ php artisan migrate
```

For development environments...

```sh
$ npm install
$ npm run dev
```
For production environments...

```sh
$ npm install --production
$ npm run prod
```
create superviser

Extra commands

```sh
$ sudo supervisorctl reread && sudo supervisorctl update && sudo supervisorctl restart [superviser-name]
```
### Used Shopify Tools

* Admin rest-api
* App-bridge

### Error messages
Unable to verify signature. = Wrong APIkeys in .env. Check the app setup in Shopify.

https://api.woomio.com/api/endpoints/RegisterTransaction?amount=1998&couponCode=4BSfF4uIC3&token=eb1872bfd9b&url=http://shopify.com&epoch=1636973526

Change this to your URL
https://iclicksee-dev-sean.myshopify.com/

https://iclicksee-dev-sean.myshopify.com/discount/ouKGIjRUCN/?token=eb1872bfd9b&coupon=ouKGIjRUCN



### Error messages
Unable to verify signature. = Wrong APIkeys in .env. Check the app setup in Shopify.

### Testing
To uninstall the app, locally while testing
1. On the shopify app  php artisan migrate:reset;  php artisan migrate;  php artisan db:seed
2. On the laravel app  delete the user relative to the store

