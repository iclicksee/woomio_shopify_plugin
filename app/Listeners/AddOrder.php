<?php

namespace App\Listeners;

use App\Constants\Api;
use App\Events\CheckOrder;
use App\Models\CampaignApiResponse;
use App\Models\Token;
use App\Models\TokenDetail;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Intl\Currencies;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AddOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CheckOrder  $event
     * @return void
     */
    public function handle(CheckOrder $event)
    {
        try {
            $ids = $event->ids;
            logger("====================START:: Add Order Listener :: " . $ids['user_id'] . " ====================");
            logger($ids['action']);

            $this->getOrders($ids['user_id'], $ids['sh_order_id']);
        } catch (\Exception $e) {
            logger("====================ERROR:: Add Order Listener ====================");
            logger($e->getMessage());
        }
    }

    public function getOrders($user, $sh_order_id)
    {
        try {
            logger('============================== START:: getOrders ==============================');

            $shop = User::where('id', $user)->first();
            $parameter['fields'] = 'id,name,note_attributes,total_price,currency';
            $endPoint = '/admin/api/' . env('SHOPIFY_API_VERSION') . '/orders/' . $sh_order_id . '.json';

            $sh_order = $shop->api()->rest('GET', $endPoint, $parameter);

            logger(json_encode($sh_order));

            $woomio['token'] = '';
            $woomio['coupon'] = '';
            $woomio['amount'] = '';
            $woomio['user_id'] = $user;
            $woomio['order_id'] = $sh_order_id;

            if (!$sh_order['errors']) {
                $sh_order = $sh_order['body']->container['order'];
                //                    $latestTo = $this->LatestExchangeRates('USD', $sh_order['currency']);
                //                    $latestFrom = $this->LatestExchangeRates('USD', 'USD');

                $woomio['amount'] = $this->calculateCurrency($sh_order['currency'], 'USD', $sh_order['total_price']);
                //                    $woomio['amount'] = round((( $sh_order['total_price'] * $latestTo ) / $latestFrom), 2);
                $woomio['currency'] = 'USD';

                if (!empty($sh_order['note_attributes'])) {
                    foreach ($sh_order['note_attributes'] as $key => $val) {
                        if ($val['name'] == 'woomio_token') {
                            $woomio['token'] = $val['value'];
                        } elseif ($val['name'] == 'woomio_coupon') {
                            $woomio['coupon'] = $val['value'];
                        }
                    }
                }
            } else {
                logger(json_encode($sh_order));
            }

            if ($woomio['token'] != '' && $woomio['coupon'] != '') {
                $this->checkToken($woomio, $sh_order, $shop);
            }
            logger('============================== END:: getOrders ==============================');
        } catch (\Exception $e) {
            logger('============================== ERROR:: getOrders ==============================');
            logger($e);
        }
    }

    public function checkToken($woomio, $sh_order, $shop = null)
    {
        try {
            logger('============================== START:: checkToken ==============================');

            $is_exist_token = Token::where('name', $woomio['token'])->where('user_id', $woomio['user_id'])->first();
            if ($is_exist_token) {
                $result = $this->sendRequest($woomio, $shop);

                logger($result);
                $newApi = new CampaignApiResponse;
                $newApi->user_id = $woomio['user_id'];
                $newApi->order_id = $woomio['order_id'];
                $newApi->token = $woomio['token'];
                $newApi->coupon = $woomio['coupon'];
                $newApi->response = $result;

                $newApi->is_success = ($result == '') ? 1 : 0;

                if ($result == '') {
                    $is_tokenDetails = TokenDetail::where('user_id', $woomio['user_id'])->where('token_id', $is_exist_token->id)->where('coupon', $woomio['coupon'])->first();

                    $tokenDetails = ($is_tokenDetails) ? $is_tokenDetails : new TokenDetail;
                    $tokenDetails->user_id = $woomio['user_id'];
                    $tokenDetails->token_id = $is_exist_token->id;
                    $tokenDetails->token = $woomio['token'];
                    $tokenDetails->coupon = $woomio['coupon'];
                    $tokenDetails->no_of_purchase = ($is_tokenDetails) ? $is_tokenDetails->no_of_purchase + 1 : 1;
                    $tokenDetails->purchase_currency = Currencies::getSymbol($woomio['currency']);
                    $tokenDetails->total_purchase_amount = ($is_tokenDetails) ? $is_tokenDetails->total_purchase_amount + $woomio['amount'] : $woomio['amount'];
                    $tokenDetails->save();
                }
                $newApi->save();
            }

            // delete token/coupon from noteAttributes
            //            $this->removeTokenFromNoteAttr($woomio['user_id'], $sh_order);
            logger('============================== END:: checkToken ==============================');
        } catch (\Exception $e) {
            logger('============================== ERROR:: checkToken ==============================');
            logger($e);
        }
    }

    public function removeTokenFromNoteAttr($user_id, $sh_order)
    {
        try {
            logger('============================== START:: removeTokenFromNoteAttr ==============================');

            $user = User::find($user_id);
            $note_attrs = [];
            if (!empty($sh_order['note_attributes'])) {
                foreach ($sh_order['note_attributes'] as $key => $val) {
                    if ($val['name'] != 'woomio_token' && $val['name'] != 'woomio_coupon') {
                        $attr['name'] = $val['name'];
                        $attr['value'] = $val['value'];
                        array_push($note_attrs, $attr);
                    }
                }
            }

            $nt = [
                'order' => [
                    'id' => $sh_order['id'],
                    'note_attributes' => $note_attrs,
                ]
            ];
            $endPoint = '/admin/api/' . env('SHOPIFY_API_VERSION') . '/orders/' . $sh_order['id'] . '.json';
            $result = $user->api()->rest('PUT', $endPoint, $nt);

            // logger(json_encode($result));
            logger('============================== END:: removeTokenFromNoteAttr ==============================');
        } catch (\Exception $e) {
            logger('============================== ERROR:: removeTokenFromNoteAttr ==============================');
            logger($e);
        }
    }

    public function sendRequest($woomio, $shop = null)
    {
        Log::debug('SendRequest');
        try {
            logger('============================== START:: sendRequest ==============================');

            
            $domain = $shop->getDomain()->toNative();
            $endPoint = 'https://api.woomio.com/api/endpoints/RegisterTransaction?amount=' . $woomio['amount'] . '&couponCode=' .  $woomio['coupon'] . '&token=' . $woomio['token'] . '&url=https://'.$domain.'&epoch='.time();

            $curl = curl_init();
            $t = (int)(time() * 1000);
            curl_setopt_array($curl, array(
                CURLOPT_URL => $endPoint,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            logger('============================== END:: sendRequest ==============================');
            return $response;
        } catch (\Exception $e) {
            logger('============================== ERROR:: sendRequest ==============================');
            logger($e);
        }
    }

    public function calculateCurrency($fromCurrency, $toCurrency, $amount)
    {
        try {
            $toCurrency = ($toCurrency == '') ? $fromCurrency : $toCurrency;
            $amount = urlencode($amount);
            $fromCurrency = urlencode($fromCurrency);
            $toCurrency = urlencode($toCurrency);
            $endPoint = "https://data.fixer.io/api/convert?access_key=" . env('DATA_FIXER_API_KEY') . "&from=$fromCurrency&to=$toCurrency&amount=$amount&format=1";
            $response = Http::get($endPoint);
            $data = json_decode($response, true);
            logger(json_encode($data));
            return (@$data['result']) ? round($data['result'], 2) : 0.00;
        } catch (\Exception $e) {
            logger($e);
        }
    }

    function LatestExchangeRates($base, $target)
    {
        try {
            $endPoint = "https://data.fixer.io/api/latest?access_key=" . env('DATA_FIXER_API_KEY') . "&base=$base&symbols=$target";
            $response = Http::get($endPoint);
            $data = json_decode($response, true);
            logger(json_encode($data));
            return (@$data['rates'][$target]) ? $data['rates'][$target] : 0.00;
        } catch (\Exception $e) {
            dd($e);
        }
    }
}
