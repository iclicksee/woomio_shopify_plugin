<?php namespace App\Jobs;

use App\Events\CheckOrder;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Objects\Values\ShopDomain;
use stdClass;

class OrdersCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain|string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain.
     * @param stdClass $data       The webhook data (JSON decoded).
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::Info("=============== Order Create Webhook ==================");
        try {
            // $this->shopDomain = ShopDomain::fromNative($this->domain);
            $shopDomain = $this->shopDomain;
            // $user = User::where('name', $shopDomain)->first();

            // event(new CheckOrder($user->id, $this->data->id, 'create'));

            return response()->json(['data' => 'success'], 200);
        } catch (\Exception $e) {
            \Log::Info('=============== ERROR:: Order Create ==================');
            \Log::Info(json_encode($e));
        }
    }
}
