<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    protected $fillable = ['coupon'];

    protected $casts = [
        'token_id' => 'integer',
        'coupon' => 'text',
    ];

    protected $table = 'coupons';
}
