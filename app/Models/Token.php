<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    use HasFactory;

    public function coupons()
    {
        return $this->hasMany(Coupon::class);
    }

    // this is a recommended way to declare event handlers
    public static function boot() {
        parent::boot();

        self::deleting(function($token) {
            $token->coupons()->each(function($coupon) {
                $coupon->delete();
            });
        });
    }
}
