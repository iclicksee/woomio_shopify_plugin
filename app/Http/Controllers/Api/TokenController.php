<?php

namespace App\Http\Controllers\Api;

use App\Events\CheckOrder;
use App\Http\Controllers\Controller;
use App\Models\Token;
use App\Models\User;
use Illuminate\Http\Request;

class TokenController extends Controller
{
    public function getToken(Request $request){
        try{
            $shop = $request->shop;
            $token = $request->token;
            $user = User::where('name', $shop)->first();

            if( $user ){
                $db_token = Token::select('name', 'expire_time', 'campaign_url', 'campaign_name')->where('user_id', $user->id)->where('name', $token)->first();
                $isSuccess = ( $db_token ) ? true : false;
                $httpCode= ( $db_token ) ? 200 : 422;
                return response()->json(['data' => ($db_token) ? $db_token : 'token not found...', 'isSuccess' => $isSuccess], $httpCode);
            }else{
                return response()->json(['data' => 'user not found...', 'isSuccess' => false], 422);
            }
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function setToken(Request $request){
        try{
            $shop = $request->shop;
            $token = $request->token;
            $coupon = $request->coupon;
            $order_id = $request->order_id;
            $user = User::where('name', $shop)->first();

            if( $user ){
                $parameter = [
                    'order' => [
                        'id' => $order_id,
                        'note_attributes' => [
                            [
                                'name' => 'woomio_token',
                                'value' => $token
                            ],
                            [
                                'name' => 'woomio_coupon',
                                'value' => $coupon
                            ]
                        ]
                    ]
                ];

                $endPoint = 'admin/orders/'. $order_id .'.json';
                $sh_order = $user->api()->rest('PUT', $endPoint, $parameter);

                if( !$sh_order['errors'] ){
                    event(new CheckOrder($user->id, $order_id, 'create'));
                }
                return response()->json(['data' => '', 'isSuccess' => true], 200);
            }else{
                return response()->json(['data' => 'user not found...', 'isSuccess' => false], 422);
            }
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }
}
