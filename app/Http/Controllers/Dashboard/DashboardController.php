<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\TokenDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class DashboardController extends Controller
{
    public function index(Request $request){
        try{
        	// dd(env('DATA_FIXER_API_KEY'));
            $user = Auth::user();

            $entity = TokenDetail::select('token', 'coupon', 'no_of_purchase', 'purchase_currency', 'total_purchase_amount')->where('user_id', $user->id)->paginate(10);

            return response()->json(['data' => $entity, 'isSuccess' => true], 200);
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }
}
