<?php

namespace App\Http\Controllers\Token;

use App\Http\Controllers\Controller;
use App\Http\Requests\TokenRequest;
use App\Models\CampaignApiResponse;
use App\Models\Coupon;
use App\Models\Token;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class TokenController extends Controller
{
    public function index(){
        try{
            $user = Auth::user();
            $tokens = Token::select('id', 'name', 'expire_time', 'campaign_url', 'campaign_name')
                ->with('coupons')
                ->where('user_id', $user->id)
                ->get()
                ->toArray();

//            $res = [];
//            foreach ( $tokens as $key=>$val ){
//                $res[$key][] = $val['id'];
//                $res[$key][] = $val['name'];
//            }
            return response()->json(['data' => $tokens, 'isSuccess' => true], 200);
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function store(TokenRequest $request){
        // dd($request->json()->all());
        // $data = [
        //     0 => [
        //       "name" => "Sean Facer",
        //       "expire_time" => 2,
        //       "campaign_name" => "dassda",
        //       "campaign_url" => "dasdsa",
        //       "discount_codes" =>  [
        //         0 => 11508770963707
        //       ]
        //     ]
        //       ];
        try{
            $user = Auth::user();
            $data = $request->json()->all();

            DB::beginTransaction();

            foreach ( $data as $key=>$val ){
                if (isset($val['id'])) {
                    $token  = $is_exist = Token::find($val['id']); 
                } else {
                    $token  =$is_exist = new Token;
                }

                $token->user_id = $user->id;
                $token->name = $val['name'];
                $token->expire_time = $val['expire_time'];
                $token->campaign_name = $val['campaign_name'];
                $token->campaign_url = $val['campaign_url'];
                $token->save();
                $token->coupons()->delete();
                foreach ($val['discount_codes'] as $coupon) {
                    $cc = new Coupon;
                    $cc->shopify_id = $coupon;
                    $cc->token_id = $token->id;
                    $save = $cc->save();;
                    
                }
                
            }

            DB::commit();
            $msg = 'Saved!';

            return response()->json(['data' => $msg, 'isSuccess' => true], 200);
        }catch( \Exception $e ){
            DB::rollBack();
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function edit(Token $token){

        $tokenFind = Token::select('id', 'name', 'expire_time', 'campaign_url', 'campaign_name')
                ->with('coupons')
                ->where('id', $token->id)
                ->get()
                ->first();
        try{
            $res[0]['id'] = $token->id;
            $res[0]['name'] = $token->name;
            $res[0]['campaign_url'] = $token->campaign_url;
            $res[0]['campaign_name'] = $token->campaign_name;
            $res[0]['expire_time'] = (string)$token->expire_time;
            $res[0]['discount_codes'] = $tokenFind->coupons;
            
            return response()->json(['data' => $res, 'isSuccess' => true], 200);
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function destroy(Token $token){
        try{
            $token->delete();
            return response()->json(['data' => 'Deleted', 'isSuccess' => true], 200);
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function test(){
        $endPoint = 'https://api.woomio.com/api/endpoints/AddCampaignCouponCode?amount=0&couponCode=9636&token=9b0dadc98cf';

        $curl = curl_init();
        $t = (int)(time() * 1000);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $endPoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
        ));
        $result = curl_exec($curl);
        curl_close($curl);

        $newApi = new CampaignApiResponse;
        $newApi->user_id = $user->id;
        if( $result == '' ){

        }else{

        }
    }

    public function shopifyCouponCodes() {
        $parameter['fields'] = 'id,name,note_attributes,total_price,currency';
        $endPoint = '/admin/api/2021-07/price_rules.json';
        $shop = User::first();
        $sh_order = $shop->api()->rest('GET', $endPoint, $parameter);
        $coupons = [];
        if (!empty($sh_order['body']['price_rules'])) {
            foreach ($sh_order['body']['price_rules'] as $priceRule) {
                $couponEndpoint = '/admin/api/2021-07/price_rules/'.$priceRule['id'].'/discount_codes.json';
                $cpn = $shop->api()->rest('GET', $couponEndpoint, $parameter);
                if (!empty($cpn['body']['discount_codes'])) {
                    foreach ($cpn['body']['discount_codes'] as $ccode) {
                        $coupons[] = $ccode['code'] ;
                    }
                }

            }
        }

        return response()->json(['data' => $coupons, 'isSuccess' => true], 200);
    }

    public function coupons(){
        $parameter['fields'] = 'id,name,note_attributes,total_price,currency';
        $endPoint = '/admin/api/2021-07/price_rules.json';
        $shop = User::first();
        $sh_order = $shop->api()->rest('GET', $endPoint, $parameter);
        $shopifycoupons = [];
        if (!empty($sh_order['body']['price_rules'])) {
            foreach ($sh_order['body']['price_rules'] as $priceRule) {
                $couponEndpoint = '/admin/api/2021-07/price_rules/'.$priceRule['id'].'/discount_codes.json';
                $cpn = $shop->api()->rest('GET', $couponEndpoint, $parameter);
                if (!empty($cpn['body']['discount_codes'])) {
                    foreach ($cpn['body']['discount_codes'] as $ccode) {
                        $shopifycoupons[] = $ccode['id'];
                        $scn[] = $ccode;
                    }
                }

            }
        }
        $coupons = Coupon::select('shopify_id')->get()->toArray();
        if (!empty($coupons)) {
            foreach($coupons as $c) {
                $carray[] = $c['shopify_id'];
            }
        } else {
            $carray = null;
        }

        

        if (!empty($carray)) {
            $return = array_diff($shopifycoupons, $carray);
        } else {
            $return = $shopifycoupons;
        }

        $data = [];
        if (!empty($return)) {
            foreach ($return as $r) {
                foreach ($scn as $ccode) {
                    if ($ccode['id'] == $r) {
                        $data[] = $ccode;
                    }
                }
            }
        }
        
        return response()->json(['data' => $data, 'isSuccess' => true], 200);
    }

    public function shopifycoupons($token_id){

        $parameter['fields'] = 'id,name,note_attributes,total_price,currency';
        $endPoint = '/admin/api/2021-07/price_rules.json';
        $shop = User::first();
        $sh_order = $shop->api()->rest('GET', $endPoint, $parameter);
        $shopifycoupons = [];
        if (!empty($sh_order['body']['price_rules'])) {
            foreach ($sh_order['body']['price_rules'] as $priceRule) {
                $couponEndpoint = '/admin/api/2021-07/price_rules/'.$priceRule['id'].'/discount_codes.json';
                $cpn = $shop->api()->rest('GET', $couponEndpoint, $parameter);
                if (!empty($cpn['body']['discount_codes'])) {
                    foreach ($cpn['body']['discount_codes'] as $ccode) {
                        $shopifycoupons[] = $ccode['id'];
                        $scn[] = $ccode;
                    }
                }

            }
        }
        $coupons = Coupon::select('shopify_id')
            ->where('token_id','!=', $token_id )->get()->toArray();
        if (!empty($coupons)) {
            foreach($coupons as $c) {
                $carray[] = $c['shopify_id'];
            }
        } else {
            $carray = null;
        }

        if (!empty($carray)) {
            $return = array_diff($shopifycoupons, $carray);
        } else {
            $return = $shopifycoupons;
        }

        $data = [];
        if (!empty($return)) {
            foreach ($return as $r) {
                foreach ($scn as $ccode) {
                    if ($ccode['id'] == $r) {
                        $data[] = $ccode;
                    }
                }
            }
        }
        
        return response()->json(['data' => $data, 'isSuccess' => true], 200);
    }

    
}
