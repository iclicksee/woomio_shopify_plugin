<?php

use Illuminate\Support\Facades\Http;

if (!function_exists('LatestExchangeRatesH')) {
    /**
     * Returns a human readable file size
     *
     * @param integer $bytes
     * Bytes contains the size of the bytes to convert
     *
     * @param integer $decimals
     * Number of decimal places to be returned
     *
     * @return string a string in human readable format
     *
     * */
    function LatestExchangeRatesH($base, $target)
    {
        try{
            $endPoint = "https://data.fixer.io/api/latest?access_key=". env('DATA_FIXER_API_KEY') ."&base=$base&symbols=$target";
            $response = Http::get($endPoint);
            $data = json_decode($response, true);
            logger(json_encode($data));
            return (@$data['rates'][$target]) ? $data['rates'][$target] : 0.00;
        }catch(\Exception $e){
            dd($e);
        }

    }
}

if (!function_exists('in_arrayi')) {

    /**
     * Checks if a value exists in an array in a case-insensitive manner
     *
     * @param mixed $needle
     * The searched value
     *
     * @param $haystack
     * The array
     *
     * @param bool $strict [optional]
     * If set to true type of needle will also be matched
     *
     * @return bool true if needle is found in the array,
     * false otherwise
     */
    function in_arrayi($needle, $haystack, $strict = false)
    {
        return in_array(strtolower($needle), array_map('strtolower', $haystack), $strict);
    }
}
